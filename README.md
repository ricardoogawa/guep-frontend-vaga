# Vaga de desenvolvedor frontend

##### Olá candidato desenvolvedor, tudo bem?
##### Veja a seguir os passos para realizar o teste para a vaga de frontend developer na GUEP.
##### Desejamos a você uma boa sorte!

# Descrição

##### Para realizar o teste de desenvolvedor frontend é necessário ter a experiência com o framework frontend Angular 9+, HTML5 & CSS3, Javascript e Typescript.

##### Neste teste é necessário que você desenvolva uma TodoList (Lista de Tarefas) com as operações de CRUD utilizando localStorage ou Sqlite.

# Diferenciais

* Fazer testes na aplicação utilizando TDD
* Fazer documentação de como executamos a aplicação
* Desenvolver fluxogramas
* Colocar a aplicação em docker






